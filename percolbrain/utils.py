from __future__ import absolute_import, division, print_function

import os
from os import fstat
from os.path import join
from sys import stdout
from math import ceil

import nibabel as nib
import graph_tool.all as gt
import numpy as np

import pdb

class ProgressBar :
    """A simple text-based progress indicator.
    Adapted from the AMICO toolbox (https://github.com/daducci/AMICO)
    """

    def __init__( self, n: int, width = 50, prefix = "", erase=False ) :
        """Initialize the progress indicator.
        Parameters
        ----------
        n : integer
            Total number of steps to track
        width : integer
            Width (in characthers) of the bar indicator (default : 50)
        prefix : string
            A string to prepend to the indicator bar (default : "")
        erase : boolean
            True -> erase the bar when done, False -> simply go to new line (default : False)
        """
        self.i      = 1
        self.n      = n
        self.width  = width
        self.nbars  = 0
        self.prefix = prefix
        self.erase  = erase
        if fstat(0)==fstat(1): #only show progress bar if stdout, False if redirected
            print('\r%s[%s] %5.1f%%' % ( prefix, ' ' * width, 0.0 ), end=' ')
            stdout.flush()


    def update( self ) :
        """Update the status and redraw the progress bar (if needed)."""
        if self.i < 1 or self.i > self.n :
            return
        ratio = float(self.i) / float(self.n)
        nbars = int( ceil(ratio * self.width) )
        if self.i == 1 or self.i == self.n or self.i or nbars != self.nbars:
            self.nbars = nbars
            if fstat(0)==fstat(1): #only show progress bar if stdout
                print('\r%s[%s%s] %5.1f%%' % ( self.prefix, '=' * self.nbars, ' ' * (self.width-self.nbars), 100.0*ratio), end=' ')
                stdout.flush()
        self.i += 1

        if self.i > self.n :
            if self.erase :
                print('\r' + ' ' * ( len(self.prefix) + self.width + 9 ) + '\r', end=' ')
            else:
                print()
            stdout.flush()

# Utils
def load_graph(fname: str, fmt: bool = False) -> gt.Graph:
    """Load a graph as a gt.Graph object

    Input:
        fname: Path to filename where the graph is placed
        fmt:   (Optional) Input file format. adj (adjacency) or edge (edge-list)

    Return:
        gt: A gt.Graph object

    """
    if os.path.exists(fname) and os.path.isfile(fname):
        if fname.endswith(".gt"):
            print(f'.  Loading input graph as GT object..')
            g = gt.load_graph(fname)

        else:
            if fmt == 'adj':
                print(f'.  Loading input as adjacency matrix..')
                adj = np.loadtxt(fname)
                idx = adj.nonzero()
                weights = adj[idx]
                g = gt.Graph()
                g.add_edge_list(np.transpose(idx))
                #add weights as an edge propetyMap
                ew = g.new_edge_property("double")
                ew.a = weights
                g.ep['edge_weight'] = ew

            elif fmt == 'edge':
                print(f'.  Loading input as edge-list..')
                g = gt.load_graph_from_csv(fname, hashed=False,
                                csv_options={'delimiter': '\t'})
            else:
                # Read
                indat = np.loadtxt(fname)

                if indat.shape[0] == indat.shape[1]:
                    print(f'.  Considering input as adjacency matrix. Loading..')
                    print(f'.  If its not the case, state in-type..')
                    idx = indat.nonzero()
                    weights = indat[idx]
                    g = gt.Graph()
                    g.add_edge_list(np.transpose(idx))
                    #add weights as an edge propetyMap
                    ew = g.new_edge_property("double")
                    ew.a = weights
                    g.ep['edge_weight'] = ew

                else:
                    print(f'.  Considering input as edge-list. Loading..')
                    print(f'.  If its not the case, state in-type..')
                    g = gt.load_graph_from_csv(fname, hashed = False, csv_options={'delimiter': ','})

    else:
        raise FileNotFoundError(f'Input file: {fname} not found')

    return g

def load_rois(fname: str) -> nib.Nifti1Image:
    """Load a nifti file of ROIs

    Input:
        fname: Path to filename where the NIfti-ROI file is located

    Return:
        nifti: A nibabel.Nifti1Image object
    """
    if os.path.exists(fname) and os.path.isfile(fname):
        if fname.endswith(".nii") or fname.endswith(".nii.gz"):
            nifti = nib.Nifti1Image.load(fname)
        else:
            raise TypeError(f'Input file should be a nifti (.nii or .nii.gz)')
    else:
        raise FileNotFoundError(f'Input Nifti file {fname} not found')

    return nifti

def is_writable(fname: str):
    """Check if output path is writable

    Input:
        fname: Path to folder where outout will be saved

    """
    d = os.path.dirname(os.path.abspath(fname))
    if os.path.exists(d):
        if os.access(d, os.W_OK):
            return
        else:
            raise PermissionError(f'User does not have permission to write output file ')
    else:
        raise FileNotFoundError(f' Output directory  {d} does not exist')

def match_graph_roi(g: gt.Graph, nifti: np.array):
    """Check if number nodes in graph match in nifti (parcellation) ROIs

    Input:
        g:       gt.Graph object
        nifti:   nibabel Nifti1Image. 3D-roi volume
    """
    numvertex = g.num_vertices()
    numrois = np.unique(nifti).size

    if numrois < numvertex:
        raise ValueError(f'Number ROIs lower than number nodes in graph. Not posible')
    elif numrois > numvertex:
        print(f'Warning! Number rois bigger than number nodes.'
                'this might be a source of error. Double check')
        print("\n [+] Considering that each row of input is a node (in order!) \n")
    else:
        print('Number ROIs match number nodes.')
        return


def match_mni_roi(nifti: nib.Nifti1Image):
    """Check to which MNI template overlaps in size.
        If do not overlap, ask for a template.

    Input:
        nifti:   nibabel Nifti1Image. 3D-roi volume
    """
    if len(os.environ.get('FSLDIR')) > 0:
        fslpath = os.environ["FSLDIR"]
        resolutions = [1,2,8]
        inshape = nifti.shape

        for resolution in resolutions:
            inmni = join(fslpath,'data','standard','MNI152_T1_'+str(resolution)+'mm_brain.nii.gz')
            loadmni = nib.Nifti1Image.load(inmni)
            if loadmni.shape == inshape:
                print(f'.  Input file dimensions match MNI dimensions')
                print(f'.  Standard space: '+'MNI152_T1_'+str(resolution)+'mm_brain.nii.gz')
                return True
        # If no match with MNI
        return False

    else:
        print(f'Warning! No FSLdir defined variable. Image not considered in MNI')
        return False

def sample_knodes(knodes: np.array, parcel: nib.Nifti1Image) -> nib.Nifti1Image:
    """
    Save top k-node from the initial Parcellation volumes.

    Input:
       knodes        :  K-nodes more vulnerable
       parcellation  :  ROIs volume where to plot

    Return:
       tosave        :  Matrix with k-nodes more vulnerable label to be saved as NiFti

    """
    fdata = parcel.get_fdata().astype(float)
    outdata = np.zeros_like(fdata)

    for idx,knode in enumerate(knodes):
        posvox = np.where(fdata == knode)
        outdata[posvox] = idx + 1

    newout = nib.Nifti1Image(outdata, parcel.affine, parcel.header)
    return newout

def sample_surface():
    """asdasd

    """

def plot_percolation():
    """asdasd

    """

# ---------------------------
# Subfunctions
def my_print(message: str, ff: str):
    """
    print message, then flush stdout
    """
    print(message)
    ff.write(message)
    sys.stdout.flush()

def run_cmd(cmd: str, err_msg: str, ff: str):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)
