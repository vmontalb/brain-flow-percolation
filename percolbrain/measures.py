from __future__ import absolute_import, division, print_function

import os
from os import fstat
from sys import stdout
from math import ceil

import graph_tool.all as gt
import numpy as np

import pdb

def high_betweenness_attack(g: gt.Graph):
    """
    Delete nodes in descending order based on betweeness
    A formal descritpion of between can be found here:
       < https://en.wikipedia.org/wiki/Betweenness_centrality >
    Inputs:
       g     : gt.Graph() object

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    Notes:
        The S parameter is based on percolation function as implemented in
    graph tool.
https://graph-tool.skewed.de/static/doc/_modules/graph_tool/topology.html#vertex_percolation

    """
    allvertices = [v for v in g.vertices()]                  # get all vertices from network
    allvertices = np.array(allvertices, dtype="uint64")      # get vertex index

    # Compute between
    bvp, bep = gt.betweenness(g)
    g.vp.betweeness = bvp                # Save as graph property for future
    order = np.argsort(bvp.a)
    orderv = allvertices[order]

    # Run percolation
    cgiant, cmp = gt.vertex_percolation(g,orderv)
    return [orderv,cgiant]


def ci_attack(g: gt.Graph, l: int):
    """
    Collective influence. Search influential nodes beyond neighbours.
    Its based on defining a baloon of size 'l', finding nodes at distance 'l'
    and computing topology-information for those nodes.

    Inputs:
       g     : gt.Graph() object. Input graph
       l     : int. Balloon size.

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    Refs:
       Monroe etal 2015 Nature

    Notes:
        The S parameter is based on percolation function as implemented in
    graph tool.
https://graph-tool.skewed.de/static/doc/_modules/graph_tool/topology.html#vertex_percolation

    """
    # Init
    g.vp.ci = g.new_vertex_property("int")
    g.vp.cidegree = g.degree_property_map("total").copy()

    # Compute CI for all vertices
    civertices = [v for v in g.vertices()]                     # get all vertices from network
    for vrtx in g.vertices():
        indexvrtx = g.vertex_index[vrtx]

        # Find neighbour
        cballoon = gt.shortest_distance(g, vrtx, target=None, max_dist = l)     # identify all nodes within balloon
        edgeballoon = np.where(cballoon.a == l)[0]                              #identify boundary baloon

        # Filter out edge-balloon-nodes previously excluded (i.e cidegree=0)
        edgeballoon = edgeballoon[g.vp.cidegree.a[edgeballoon] > 0]

        # Compute CI
        if g.vp.cidegree.a[indexvrtx] > 0:
            cci = (g.vp.cidegree.a[indexvrtx] - 1) * np.sum(g.vp.cidegree.a[edgeballoon] - 1) # Compute Collective Influence
        else:
            cci = 0
        g.vp.ci[vrtx] = cci

    # Order
    allvertices = [v for v in g.vertices()]                     # get all vertices from network
    allvertices = np.array(allvertices, dtype="uint64")         # get vertex index
    order = np.argsort(g.vp.ci.a)
    orderv = allvertices[order]

    # Run percolation attack
    cgiant, cmp = gt.vertex_percolation(g,orderv)      # Run percolation

    return [orderv,cgiant]

def module_connectors_attack(g: gt.Graph):
    """
    Detect nodes that are connecting different modules and attack them
    The attack is based on their betweeness centrality measure.

    Inputs:
       g     : gt.Graph() object. Input graph

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    Refs:
       Cunha etal 2015 Plos One

    Notes:
        - The community detection method applied is based on SBM (by Peixoto et al)
        It uses the function gt.minimize_blockmodel_dl(g)
        Since its an stochastic model, slight differences at every run might happen

        - The S parameter is based on percolation function as implemented in
    graph tool.
https://graph-tool.skewed.de/static/doc/_modules/graph_tool/topology.html#vertex_percolation
    """
    # Run SBM module clustering
    print(f'.   Computing SBM clustering')
    clust = gt.minimize_blockmodel_dl(g)
    g.vp.module = clust.get_blocks()

    # Identify between-blocks connectors
    connectors = []
    for cv in g.vertices():
        # Get current vertex module
        cblock = g.vp.module[cv]

        # Iterate over neighbours
        for cneigh in cv.all_neighbors():
            # Check if one of neighbours is from a different block/module
            if g.vp.module[cneigh] != cblock:
                connectors.append(g.vertex_index[cv])
                break

    # Compute betweeness and descend-order the connectors
    bvp, bep = gt.betweenness(g)
    g.vp.betweeness = bvp
    betweenconn = bvp.a[connectors]
    order = np.argsort(betweenconn)
    reorderconn = [connectors[ii] for ii in order]

    # Create fake vector.
    # Not all nodes will be connectors, need to pass all vertices to percol
    allvertices = [v for v in g.vertices()]
    allvertices = np.array(allvertices, dtype="uint64")
    tofill = np.setdiff1d(allvertices,np.array(reorderconn, dtype="uint64"))
    orderv = np.concatenate((tofill,np.array(reorderconn, dtype="uint64")), axis = 0, dtype="uint64")

    # Run percolation attack
    print(f'.   Run betweeness module-interconnectors attack')
    cgiant, cmp = gt.vertex_percolation(g,orderv)
    modulepercol = cgiant

    # Fill non-connectors left-over with fake giant comp. THey are not used
    tocte = len(connectors)
    lastgiant = cgiant[-(tocte+1)]
    cgiant[0:-tocte] = lastgiant

    return [orderv,cgiant]

def degree_attack(g: gt.Graph, mod: str='total'):
    """
    Delete nodes in descending order based on total degree

    Inputs:
       g     : gt.Graph() object. Input graph

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    Notes:
        The S parameter is based on percolation function as implemented in
    graph tool.
https://graph-tool.skewed.de/static/doc/_modules/graph_tool/topology.html#vertex_percolation

    """
    allvertices = [v for v in g.vertices()]                  # get all vertices from network
    allvertices = np.array(allvertices, dtype="uint64")      # get vertex index

    # Compute degree
    if mod == 'in':
        nodedegree = g.degree_property_map("in").a
    elif mod=='out':
        nodedegree = g.degree_property_map("out").a
    else:
        nodedegree = g.degree_property_map("total").a

    # Order
    order = np.argsort(nodedegree)
    orderv = allvertices[order]

    # Run percolation
    print(f'.   Run degree-based attack')
    cgiant, cmp = gt.vertex_percolation(g,orderv)      # Run percolation

    return [orderv,cgiant]

def heuristic_ci():
    """asdasd

    """

def random_percolation(g: gt.Graph, n: int = 500):
    """
    Percolation attacking random nodes at a time
    It will create a null-model based on n-iterations
    The median value will be stored

    Inputs:
       g     : gt.Graph() object. Input graph
       n     : int. Number iterations

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    """
