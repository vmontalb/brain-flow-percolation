# Brain Flow Percolation

Computing multiple network-measure from brain networks and study an "attack"/percolation based on such measure.


To-Do list:
----------- 
- [X] - Code Percolation measures
- [X] - Sample to parcellation nodes
- [ ] - Curate parcellation nodes nomenclature (from node 0 to Num-Nodes)
- [ ] - More testing on the sampling to Niifti nodes
- [ ] - Generate automatic report (with SS)
