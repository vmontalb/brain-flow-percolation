#!/usr/bin/env python
# percolbrain.py
"""
Author: Victor Montal - victor.montal [at] protonmail.com
Date init: 23 July 2021
"""
import os
from os.path import join
import logging
import argparse
import getpass

import nibabel as nib
import numpy as np

import percolbrain.measures
import percolbrain.utils

import pdb

# ---------------------------
# Parser Options
HELPTEXT = """

diffusion_pipeline.py [dev version]

Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat


>> <<

STEPS:
------
    0.- Generate folder structure and debugger file.
    1 - Motion Correction (eddy correct + bvecs rotation)

REFS:
-----

[1] Greve DN et al. Neuroimage 2009. doi: 10.1016/j.neuroimage.2009.06.060

"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_graph   = f'.txt file with subjects to process. one-by-row.'
    h_gtype   = f'Type of input graph. It can be adj (adjancency) or edge (edgelist).'
    h_parcel  = f'Path where the DWI data is placed ready to processed.'
    h_knode   = f'Number of vulnerable/important nodes to overlay to brain imagge '
    h_outp    = f'Path where output will be saved'
    h_debug   = f'Debug mode ON'

    parser.add_argument('--network',        dest = 'networkf',  action = 'store',       help = h_graph,     required = True)
    parser.add_argument('--net-type',       dest = 'typenet',   action = 'store',       help = h_gtype,     required = False, default = 'None')
    parser.add_argument('--parcellation',   dest = 'inrois',    action = 'store',       help = h_parcel,    required = True)
    parser.add_argument('--num-nodes',      dest = 'knodes',    action = 'store',       help = h_knode,     required = False, default = 5, type=int)
    parser.add_argument('--outpath',        dest = 'outpath',   action = 'store',       help = h_outp,      required = True)
    parser.add_argument('--debug',          dest = 'debug',     action = 'store_true',  help = h_debug,     required = False, default = False)

    args = parser.parse_args()
    return args


# ---------------------------
# Main Code
def brain_percol(options: argparse):
    """
    Steps:
    0.- Load graph, nifti (parcellation/ROI)
    1.- Sanitize inputs
    2.- Compute percolation
        2.1- Betweenessness
        2.2- Degree
        2.3- Between-module betweeness
        2.4- Collective Influence
    3.- Sample k-nodes from each percolation method to volume
    4.- Sample k-nodes from each percolation method to surface
    5.- Save nifti output
    6.- Screenshot to .pdf file
    """

    # Set logging
    logfile = join(options.outpath,'percolbrain.log')
    percolbrain.utils.is_writable(options.outpath)

    uname = getpass.getuser()
    if options.debug is True:
        logging.basicConfig(
            level=logging.DEBUG,
            format=f'%(asctime)s [%(levelname)s] - {uname} - %(message)s',
            datefmt='%m/%d/%Y %I:%M:%S %p',
            handlers=[
                logging.FileHandler(logfile),
                logging.StreamHandler()
            ]
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format=f'%(asctime)s [%(levelname)s] - {uname} - %(message)s',
            datefmt='%m/%d/%Y %I:%M:%S %p',
            handlers=[
                logging.FileHandler(logfile),
                logging.StreamHandler()
            ]
        )

    # Load graph
    logging.info(f'Loading input graph..')
    logging.debug(f'input file: {options.networkf}')
    g = percolbrain.utils.load_graph(options.networkf, options.typenet)

    # Load parcellation
    logging.info(f'Loading parcellation file..')
    logging.debug(f'input parcellation file: {options.inrois}')
    parcel = percolbrain.utils.load_rois(options.inrois)
    fdata = parcel.get_fdata().astype(float)
    logging.debug(f'\
                    \n   Parcellation Size: {parcel.shape} \
                    \n   Parcellation ROIs: {str(np.unique(fdata)-1)}')

    # Sanitize input
    logging.info(f'Checking validity input options')
    percolbrain.utils.match_graph_roi(g, fdata)
    ismni = percolbrain.utils.match_mni_roi(parcel)

    # Percolation
    # Betweeness
    logging.info(f'Starting percolation attack')
    logging.info(f'Betweeness-based percolation attack')
    betweenorderv,betweencgiant = percolbrain.measures.high_betweenness_attack(g)
    half = np.sum(betweencgiant < betweencgiant[-1]/2)
    numhalf = betweencgiant.size - half
    logging.debug(f'   Largest cluster size after removing one node: '+str(betweencgiant[-2]))
    logging.debug(f'   Number nodes removed for half LCC: '+str(numhalf))

    # Degree
    logging.info(f'Degree-based percolation attack')
    degreeorderv,degreecgiant = percolbrain.measures.degree_attack(g)
    half = np.sum(degreecgiant < degreecgiant[-1]/2)
    numhalf = degreecgiant.size - half
    logging.debug(f'   Largest cluster size after removing one node: '+str(degreecgiant[-2]))
    logging.debug(f'   Number nodes removed for half LCC: '+str(numhalf))

    # Module-betweeness
    logging.info(f'Module-betweeness-based percolation attack')
    moduleorderv,modulecgiant = percolbrain.measures.module_connectors_attack(g)
    half = np.sum(modulecgiant < modulecgiant[-1]/2)
    numhalf = modulecgiant.size - half
    logging.debug(f'   Largest cluster size after removing one node: '+str(modulecgiant[-2]))
    logging.debug(f'   Number nodes removed for half LCC: '+str(numhalf))

    # Collective Incluence
    logging.info(f'Collective Influence (static) percolation attack')
    ciorderv,cicgiant = percolbrain.measures.ci_attack(g, l=3)
    half = np.sum(cicgiant < cicgiant[-1]/2)
    cihalf = cicgiant.size - half
    logging.debug(f'   Largest cluster size after removing one node: '+str(cicgiant[-2]))
    logging.debug(f'   Number nodes removed for half LCC: '+str(numhalf))

    # Sampling to volume
    logging.info(f'Generating NifTi vols of top percolated {str(options.knodes)} nodes')
    # Save betweeness percolation
    tosave = join(options.outpath,'between.knodes.nii.gz')
    knodelist = betweenorderv[-options.knodes:]
    knodelist = np.flip(knodelist) + 1
    logging.info(f'.  Top k-nodes for betweeness: '+str(knodelist))
    savevol = percolbrain.utils.sample_knodes(knodelist, parcel)
    nib.save(savevol, tosave)


    # Sampling to surface fsaverage (if MNI)
    logging.info(f'Generating Surface vols of top percolated {str(options.knodes)} nodes')

    # Report
    logging.info(f' Generating final .pdf report')






# ---------------------------
# Run code
if __name__ == "__main__":
    options = options_parser()
    brain_percol(options)
